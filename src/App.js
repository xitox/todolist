import React from 'react';
import NavBar from "./function/NavBar/NavBar.tsx";
import './App.scss';
import AddTask from "./function/AddTask/AddTask";
import DisplayTask from "./function/DisplayTask/DisplayTask";

function App() {
  return (
    <div className="App">
      <NavBar />
      <div className="main-box">
        <AddTask />
        <DisplayTask />
      </div>
    </div>
  );
}

export default App;
