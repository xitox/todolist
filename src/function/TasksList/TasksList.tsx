import React, { useEffect, useState } from 'react';
import OneRow from '../OneRow/OneRow';
import './TasksList.scss';

const TasksList = () => {
    const [data, setData] = useState([]);
    const [change, setChange] = useState();

    useEffect(() => {
        fetch('/api')
            .then(res => res.json())
            .then(setData);
    }, [change]);


    let rows: string[] = [];
    for (let i in data) {
        rows.push(data[i]);
    }
    let container = rows.map((task, i) => {
        return <OneRow setData={setChange} oneTask={task} key={i} />;
    });

    return (
        <tbody>
            {container}
        </tbody>
    );
};

export default TasksList;