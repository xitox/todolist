import React from 'react';
import './NavBar.scss';

const NavBar = () => {
    return (
        <div className="navBar">
            <h1>To do list Web App</h1>
        </div>
    );
};

export default NavBar;