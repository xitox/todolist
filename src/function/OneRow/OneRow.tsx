import React from 'react';
import './OneRow.scss';

const OneRow = (props: any) => {
    const deleteTask = () => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id: props.oneTask.id,
            })
        };
        fetch('/deleteTask', requestOptions)
            .then((res) => res.json())
            .then(props.setChange);
        window.location.reload();
    }

    return (
        <tr>
            <td className='title-cell'>{props.oneTask?.title}</td>
            <td className='desc-cell'>{props.oneTask?.description}</td>
            <td className='button-cell'><button onClick={deleteTask}>X</button></td>
        </tr>
    );
};

export default OneRow;