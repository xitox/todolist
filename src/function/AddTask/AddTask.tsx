//import axios from 'axios';
import React from 'react';
import './AddTask.scss';

const AddTask = () => {
    let titleChanged = false;
    let descChanged = false;
    let clickHandleTitle = () => {
        if (!titleChanged)
            (document.getElementsByClassName('title-input').item(0) as HTMLInputElement).value = '';
        titleChanged = true;
    }
    let clickHandleDesc = () => {
        if (!descChanged)
            (document.getElementsByClassName('description-input').item(0) as HTMLInputElement).value = '';
        descChanged = true;
    }
    let addTache = () => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                title: (document.getElementsByClassName('title-input').item(0) as HTMLInputElement).value,
                description: (document.getElementsByClassName('description-input').item(0) as HTMLInputElement).value,
            })
        };
        fetch('/addTask', requestOptions)
            .then((res) => res.json());
        window.location.reload();
    }

    return (
        <div className="add-task">
            <input type='text' name='title' onFocus={clickHandleTitle} defaultValue='title...' className='title-input' />
            <textarea name='description' onFocus={clickHandleDesc} defaultValue='Description...' className='description-input' ></textarea>
            <button type='button' onClick={addTache} className='bouton-save'>Save</button>
        </div>
    );
};

export default AddTask;