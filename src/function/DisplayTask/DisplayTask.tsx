import React from 'react';
import TasksList from '../TasksList/TasksList';
import './DisplayTask.scss'

const DisplayTask = () => {
    return (
        <div className="display-task">
            <table className='table-task'>
                <thead>
                    <tr className='table-head'>
                        <th className='title-head'>Title</th>
                        <th className='desc-head'>Description</th>
                        <th className='delete-head'>Delete</th>
                    </tr>
                </thead>
                <TasksList />
            </table>
        </div>
    );
};

export default DisplayTask;